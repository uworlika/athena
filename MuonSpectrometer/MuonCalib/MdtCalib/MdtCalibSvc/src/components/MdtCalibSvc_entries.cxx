#include "MdtCalibSvc/MdtCalibrationSvc.h"
#include "MdtCalibSvc/MdtCalibrationRegionSvc.h"
#include "MdtCalibSvc/MdtCalibrationDbSvc.h"
#include "MdtCalibSvc/MdtCalibrationT0ShiftSvc.h"                                                                                                                                                     
#include "MdtCalibSvc/MdtCalibrationTMaxShiftSvc.h"

DECLARE_COMPONENT( MdtCalibrationSvc )
DECLARE_COMPONENT( MdtCalibrationRegionSvc )
DECLARE_COMPONENT( MdtCalibrationDbSvc )
DECLARE_COMPONENT( MdtCalibrationT0ShiftSvc )
DECLARE_COMPONENT( MdtCalibrationTMaxShiftSvc )

